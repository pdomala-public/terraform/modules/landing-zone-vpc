# VPC
resource "aws_vpc" "vpc" {
  cidr_block           = var.vpc_cidr
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags                 = var.tags
}

data "aws_region" "current" {}
data "aws_availability_zones" "available" {}

resource "aws_subnet" "public" {
  count             = length(data.aws_availability_zones.available.names)
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = cidrsubnet(aws_vpc.vpc.cidr_block, 4, count.index)
  availability_zone = data.aws_availability_zones.available.names[count.index]
  tags              = merge(local.tags, { "Name" = "public_${terraform.workspace}_${count.index}" })

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_subnet" "private" {
  count             = length(data.aws_availability_zones.available.names)
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = cidrsubnet(aws_vpc.vpc.cidr_block, 4, count.index + 6)
  availability_zone = data.aws_availability_zones.available.names[count.index]
  tags              = merge(local.tags, { "Name" = "private_${terraform.workspace}_${count.index}" })

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_subnet" "database" {
  count             = length(data.aws_availability_zones.available.names)
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = cidrsubnet(aws_vpc.vpc.cidr_block, 4, count.index + 12)
  availability_zone = data.aws_availability_zones.available.names[count.index]
  tags              = merge(local.tags, { "Name" = "database_${terraform.workspace}_${count.index}" })

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_vpc_endpoint" "s3" {
  vpc_id          = aws_vpc.vpc.id
  service_name    = "com.amazonaws.${data.aws_region.current.name}.s3"
  route_table_ids = [aws_vpc.vpc.main_route_table_id]
}

# resource "aws_nat_gateway" "nat_gw" {
#   count         = length(data.aws_availability_zones.available.names)
#   allocation_id = aws_eip.nat[count.index].id
#   subnet_id     = aws_subnet.public[count.index].id
# }

# resource "aws_eip" "nat" {
#   count = length(data.aws_availability_zones.available.names)
#   vpc   = true
# }

# resource "aws_internet_gateway" "internet" {
#   vpc_id = aws_vpc.vpc.id
#   tags   = merge(local.tags, { "Name" = "igw_${terraform.workspace}" })
# }

/* Public */
# resource "aws_route_table" "public" {
#   count  = length(data.aws_availability_zones.available.names)
#   vpc_id = aws_vpc.vpc.id
#   tags   = merge(local.tags, { "Name" = "public_route_table_${count.index}" })
# }

# resource "aws_route_table_association" "public" {
#   count          = length(data.aws_availability_zones.available.names)
#   subnet_id      = aws_subnet.public[count.index].id
#   route_table_id = aws_route_table.public[count.index].id
# }

# resource "aws_route" "public_default" {
#   count                  = length(data.aws_availability_zones.available.names)
#   route_table_id         = aws_route_table.public[count.index].id
#   gateway_id             = aws_internet_gateway.internet.id
#   destination_cidr_block = "0.0.0.0/0"
# }

/* Private */
resource "aws_route_table" "private" {
  count  = length(data.aws_availability_zones.available.names)
  vpc_id = aws_vpc.vpc.id
  tags   = merge(local.tags, { "Name" = "private_route_table_${count.index}" })
}

resource "aws_route_table_association" "private" {
  count          = length(data.aws_availability_zones.available.names)
  subnet_id      = aws_subnet.private[count.index].id
  route_table_id = aws_route_table.private[count.index].id
}

# resource "aws_route" "private_default" {
#   count                  = length(data.aws_availability_zones.available.names)
#   route_table_id         = aws_route_table.private[count.index].id
#   nat_gateway_id         = aws_nat_gateway.nat_gw[count.index].id
#   destination_cidr_block = "0.0.0.0/0"
# }

/* Database */
resource "aws_route_table" "database" {
  count  = length(data.aws_availability_zones.available.names)
  vpc_id = aws_vpc.vpc.id
  tags   = merge(local.tags, { "Name" = "database_route_table_${terraform.workspace}_${count.index}" })
}

resource "aws_route_table_association" "database" {
  count          = length(data.aws_availability_zones.available.names)
  subnet_id      = aws_subnet.database[count.index].id
  route_table_id = aws_route_table.database[count.index].id
}

# resource "aws_route" "database_default" {
#   count                  = length(data.aws_availability_zones.available.names)
#   route_table_id         = aws_route_table.database[count.index].id
#   nat_gateway_id         = aws_nat_gateway.nat_gw[count.index].id
#   destination_cidr_block = "0.0.0.0/0"
# }