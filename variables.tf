variable "region" {
  default = "ap-southeast-2"
}

variable "vpc_cidr" {
  description = "CIDR Block for the VPC"
}

variable "application" {
  description = "Application or Service Name"
}

locals {
  default_tags = { service = var.application }
  tags         = merge(local.default_tags, var.tags)
}

variable "tags" {
  type    = map(string)
  default = {}
}
